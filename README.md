# Bienvenidos a Marsbot

## Employee Handbook

Este documento es nuestro **employee handbook**. Aquí podrás encontrar toda la
información necesaria para entender el funcionamiento de la empresa. Cada una
de las áreas de trabajo posee una sección o un manual propio. A lo largo de
estos documentos podrás entender lo que hacemos como equipo. Este manual no
representa ningún tipo de contrato, política empresarial o esquema de
trabajo. En lugar de ello debe considerarse como una guía que nos permite
saber en donde estamos, que es lo que tratamos de conseguir, como pretendemos
lograrlo, porque lo estamos haciendo y, lo más importante de todo, que nos
ayude a cumplir nuestras metas.

## Contenido

1. [Introducción](#introducción)
  * [Propósito del documento](#propósito-del-documento)
  * [Valores](#valores)
  * [Uso de este manual](#uso-de-este-manual)
  * [Directrices generales](#directrices-generales)

2. [Comunicación](#comunicación)
  * [Chat](#chat)
  * [Videollamadas](#videollamadas)
  * [Documentos](#documentos)
  * [Lista de correos](#lista-de-correos)
  * [Juntas presenciales](#juntas-presenciales)


## Introducción

### Propósito del documento

Marsbot es una empresa cuyo objetivo es crear sistemas web que mejoren la
imagen de nuestros clientes. Como parte de nuestros servicios también incluimos
el manejo de redes sociales y la creación de estrategias de contenidos que les
permitan posicionarse en el internet.

Las principales razones por las cuales existe este documento son:

* Leer es más rápido que escuchar.

* Leer es un proceso individual por lo que no se depende de alguien más para
conocer los objetivos del grupo.

* Tener un manual hace más fácil la capacitación de cualquier persona que se
incorpore al equipo.

* El trabajo es más sencillo cuando se describen, de forma clara y abierta,
los procesos que lo componen.

* El trabajo en equipo es más sencillo cuando se conoce la forma en que
trabajan los demás.

* Los cambios en la forma de trabajar se pueden analizar mejor cuando se
proponen por escrito.

* Todas las discusiones pueden tomar como punto de partida los acuerdos
hechos con anterioridad.

* Todos pueden conocer cualquier decisión tomada cuando esta sea relevante
para el equipo.

Este manual está diseñado para ser usado a largo plazo. Aquí se establecen los
principios básicos bajo los cuales se lleva a cabo el trabajo en equipo. Este
documento puede y debe modificarse tan seguido como sea necesario. Su contenido
**refleja en todo momento** la forma de pensar del equipo en conjunto.

### Valores

Nosotros valoramos los resultados, la comunicación, la colaboración,
la honestidad, la diversidad, los estándares y la amabilidad:

* **Resultados:** El esfuerzo de cada uno de los integrantes del equipo se puede
apreciar en los resultados. Cada logro debe celebrarse y nuestro trabajo debe
ser reconocido en base a estos. No importa cuanto tiempo o personas se hayan
necesitado para conseguir algo, cada resultado es una muestra de que nuestros
proyectos avanzan y deben ser compartidos con el equipo. La razón por la que
compartimos nuestros resultados es para que nuestro éxito nos motive a seguir
trabajando y no ver nuestras actividades sólo como una obligación.

* **Comunicación:** La base de cualquier relación personal y laboral sana es una
buena comunicación. Todas las ideas son valiosas sin importar quien, como,
cuando o porque se digan. El hecho de que todos respetemos la opinión del
otro nos permite hablar de forma abierta y compartir nuestros puntos de vista.
De esta manera podemos estar seguros de que nuestro equipo en verdad es un
equipo y no somos solo un grupo de personas trabajando en el mismo proyecto.

* **Colaboración:** El trabajo en equipo requiere que todos estén dispuestos a
colaborar entre sí. Cualquiera puede pedir ayuda cuando sea necesario y se
espera que los demás ofrezcan su apoyo. Cuando la ayuda necesaria no esté
dentro de nuestras áreas de dominio lo mejor que podemos hacer es tratar de
aconsejar lo que haríamos en esa situación y mencionar a la persona que sí
tenga conocimiento del tema en cuestión. Quien esté trabajando en una
actividad específica tiene el derecho a tomar la última decisión respecto a
la misma, pero siempre debe hacerlo considerando la opinión del resto del
equipo cuando exista una.

* **Honestidad:** Antes que ser compañeros de trabajo, nos vemos a nosotros mismos
como amigos y por esa razón esperamos que los demás nos entiendan y brinden
su apoyo. Cualquier actividad que realicemos puede salir mal, ya sea por
errores propios o por situaciones que están fuera de nuestras manos. No hay
ninguna razón para ocultar una falla, al contrario, hablar de forma abierta
con los demás permite ayudarnos mutuamente y continuar creciendo como equipo.
No importa si es un tema laboral o personal, podemos confiar en nosotros
mismos y ser honestos los unos con los otros. Esto también implica que debemos
estar abiertos a la crítica y reconocer que se hace más daño al ocultar la
verdad que al decirla.

* **Diversidad:** Nosotros conformamos un equipo unido, pero al mismo tiempo
reconocemos la individualidad de cada persona. El respeto a las distintas
formas de pensar, los gustos, las preferencias y las decisiones personales
debe prevalecer por encima de cualquier decisión grupal, siempre y cuando estas
no nos afecten en conjunto. Cuando estemos en desacuerdo se debe buscar la
manera de mediar nuestros puntos de vista y mantener una posición tolerante
en todo momento. La convivencia en equipo tiene como uno de sus pilares
básicos el respeto a la diferencia que existe entre todos nosotros.

* **Estándares:** Cualquier proceso que realicemos debe ser estandarizado, o al
menos se debe intentar hacerlo. Esto asegura la fácil replicación del trabajo.
Las normas no solo deben ser internas. Cualquier estándar externo debe ser
considerado e implementado para asegurar la máxima calidad de nuestros
servicios y/o productos. Tener una serie de procesos bien documentados permite
su análisis posterior. De esta manera podemos medir el impacto que genera en
nuestro rendimiento cualquier cambio aplicado a nuestro esquema de trabajo.

* **Amabilidad:** Nosotros trabajamos juntos por gusto y con gusto. Cada vez que se
revise el trabajo de otra persona es importante reconocer los puntos positivos.
Cuando se quieran señalar fallas lo preferible es hacerlo en privado, siempre
juzgando el trabajo y dando ejemplos puntuales de donde se han cometido
errores. Todos podemos pasar por momentos difíciles en nuestra vida personal.
El respetar a los demás cuando pasan por una mala situación y aprender a
moderarse cuando nos sucede a nosotros mismos es muy importante para asegurar
que el tiempo que pasamos juntos sea lo más agradable posible.

### Uso de este manual

Este manual contiene las reglas básicas bajo las cuales trabaja el equipo.
Antes de tomar alguna decisión o empezar cualquier actividad se debe
considerar lo siguiente:

* Cualquier tema que se discuta entre los miembros del equipo puede ser de
interés general. Al terminar la conversación vale la pena preguntarse en
dónde deberían quedar registradas las conclusiones y quien debe hacerlo.
¿Quién va a documentar lo que acabamos de hablar? Este es el último punto
que se debe acordar antes de dar por terminado un tema.

* Cuando se comparta información siempre se debe tratar de incluir la
fuente original. Esto permite que el resto del equipo la revise de acuerdo
a sus propios tiempos. Dependiendo del tipo de información se debe: subir
archivos a la nube, compartir links, sacar copias de documentos o realizar
cualquier otro procedimiento necesario para que la información se encuentre
siempre disponible. En caso de necesitar estos materiales solo será necesario
preguntarle a la persona responsable donde se pueden encontrar o, si no están
disponibles, si puede hacerlos públicos.

* Todos los cambios de esta guía serán realizados de forma colaborativa. Los
cambios se pueden proponer mediate el uso de issues o a través de
modificaciones directas. Para incluir modificaciones solo será necesario crear
un merge request con los cambios correspondientes. Dentro del mensaje de commit
se puede justificar el cambio. Cuando se realice una junta grupal se debe
mencionar esta edición. Una vez que se anuncie un cambio, y este se considere
aceptable, una persona distinta a la que lo propuso debe aceptar el merge
request. Si el cambio se rechaza el merge request se cerrará sin aplicarlo.

* Recordar que este documento es una guía y, aunque contiene reglas e
indicaciones de trabajo, cada quien puede aplicar su propio criterio a la
hora de hacer las cosas. Un buen motivo para proponer un cambio es encontrar
una mejor forma de realizar el trabajo.

### Directrices generales

1. Pertenecer a Marsbot significa colaborar con las personas más talentosas
con las que se puede trabajar, desarrollar los productos más interesantes,
vivir las experiencias más emocionantes que se puedan tener, compartir sueños,
metas y logros. Dicho de otro modo, lo que sucede en Marsbot, más que un
trabajo, es una experiencia de vida.

2. Nosotros reconocemos el valor de la inspiración. Si algo que te apasiona
puede generar resultados trabaja en ello.

3. Haz lo que sea mejor para nuestros clientes y usuarios, siempre pensando en
el largo plazo.

4. Nosotros creamos soluciones simples para situaciones complejas.

5. Nosotros siempre usamos soluciones simples para tareas complejas.

6. Cuando existan dudas o problemas el equipo hará todo lo posible por encontrar
respuestas y soluciones. Lo más importante del proceso es compartir nuestros
retos para poder afrontarlos juntos.

7. Como parte del equipo cada persona domina uno o varios temas, pero no todos.
Es normal que de vez en cuando alguien ignore cosas que salen fuera de su área.
Cuando esto suceda lo más natural es admitir que desconocemos el tema. Nadie
va a juzgarnos por lo que no sabemos y de esta forma los demás pueden
ayudarnos.

8. Todos los procedimientos y plantillas deben ser compartidos. Esto permite 
que sea más fácil encontrar la información cuando se necesite.

9. Cada avance realizado debe compartirse con todos los demás.

10. Cuando se cometa un error lo que se debe hacer es dar aviso al resto
del equipo, proponer una solución, trabajar en ello y, si es necesario,
modificar los procesos para evitar que ocurra de nuevo. 

11. Para nosotros fallar significa no solucionar los problemas. Los errores son
oportunidades para aprender y mejorar.

12. Siempre es importante tomar en cuenta estas directrices al estar trabajando.
En caso de existir dudas en cuanto a su significado se pueden discutir de
forma abierta con el resto del equipo.

13. Si algo te molesta platicalo de inmediato con tu compañero más próximo, y
con el resto del equipo en cuanto sea posible. Queremos que las molestias
desaparezcan antes de que se conviertan en problemas.

14. Somos un equipo integrado por las mejores personas que conocemos. Si conoces
a alguien mejor que tu en algo nos encantaría tratar de que se una al equipo.

15. Recuerda que todos nos esforzamos para dar lo mejor pero que algunas
situaciones están fuera de nuestro control. Cuando algo no esté funcionando
pregunta cómo puedes apoyar en lugar de preguntar porque no está funcionando.

16. Un grupo de trabajo no es una democracia. Cuando tomes una decisión da aviso
a los demás, pero no esperes su aprobación para actuar. Todos confiamos en
que las decisiones tomadas serán en beneficio de todos.

17. Lo más importante para el equipo son los resultados, no importa la forma en
que estos se logren. Si deseas tomarte la tarde libre o realizar actividades
que nada tengan que ver con el trabajo puedes hacerlo. Si el trabajo se
entrega, tienes libertad de usar tu tiempo como más te convenga.

18. Todo está en un estado de mejora continua, incluyendo esta guía. Cuando
inicies una nueva actividad, documenta tu trabajo mientras lo haces. Un
borrador nos permite conocer el desarrollo de las actividades por lo que es
igual de importante que un reporte de avances o un reporte final.

19. Cualquier propuesta de trabajo debe mencionar las actividades clave a
realizar, la acción inicial del proceso y quien debería realizarla.

20. Cuando se asigne una tarea, quien se encarge de ella debe proponer un
tiempo aproximado para completarla y dar aviso cuando termine.

## Comunicación

No es necesario que el equipo esté junto en un mismo lugar para poder
trabajar. Por esta razón es importante definir la forma en que nos
comunicamos y compartimos información. Los medios de comunicación que
utilizaremos son chat (whatsapp), videollamadas (hangouts),
documentos (drive), lista de correos (google groups) y juntas presenciales.

### Chat

El whatsapp puede utilizarse para comunicación grupal o individual. Cuando la
comunicación sea individual esta será autorregulada. No hay ningún tipo de
norma o sugerencia sobre cómo se debe utilizar el chat en este caso.

Cuando se utilice para comunicación grupal se deben considerar lo siguiente:

* Enviar avisos de los eventos y actividades importantes permite que todos
estemos enterados de lo que ocurre.

* Preguntar por los eventos y actividades permite recordar lo que falta
por hacer.

* El chat es un medio de comunicación en tiempo real que permite solucionar
problemas urgentes, por lo que un tema que no es urgente se puede analizar
con más calma en otro medio.

* Dentro del chat se pueden proponer o compartir temas de interés para
ser discutidos.

* Si un asunto en la conversación pasa de unas cuantas líneas es importante
considerar si se debe detener el tema y programarlo para una junta o pasar
a una videollamada para continuar la plática.

### Videollamadas

Es deseable que se realice 1 videollamada por semana por cada proyecto para
conocer el estado de las actividades. El objetivo de esta videollamada es
revisar los avances que se han dado, discutir los problemas que han aparecido
e intercambiar ideas que permitan solucionarlos. Cuando se realicen varios
proyectos de forma simultánea, estos se pueden tratar en la misma videollamada.

### Documentos

Los distintos procesos realizados por el equipo deben ser documentados. Todas
la plantillas y formatos utilizados para trabajar deben encontrarse en un
lugar centralizado que permita el fácil acceso.

### Lista de correos

Los temas que requieran un análisis más a fondo pueden ser enviados a una
lista de correo. Cada correo debe contener un tema específico para que sea
sencillo seguir el hilo de la discusión. Al usar el correo electrónico nos
aseguramos que cada persona pueda revisar su contenido, pensar en las
posibles respuestas y tomar el tiempo necesario para responder. De ninguna
manera se espera que las discusiones dentro de la lista se den a la misma
velocidad que las que se realizan utilizando otros medios.

### Juntas presenciales

De forma mensual se debe realizar al menos una junta presencial. A diferencia
de las videollamadas semanales, en estas juntas debe existir una agenda de
temas. Estas juntas son el equivalente a una junta del consejo directivo
donde se toman decisiones respecto a las acciones que se deben priorizar,
discutir cambios drásticos en la forma de trabajar, se hagan observaciones
financieras, se determine el desarrollo y crecimiento de la empresa, etc.